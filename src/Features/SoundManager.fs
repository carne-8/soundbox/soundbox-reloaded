module SoundManager

open Fable.Core
open Fable.ReactNative.FileSystem

type Sound =
    { Name: string
      Sound: Fable.ReactNative.Sound.Sound
      Image: string
      IsPlaying: bool }


let downloadSound soundFolderPath soundName soundUrl =
    promise {
        let newFileFolder =
            sprintf "%s/%s"
                soundFolderPath
                soundName

        let newFilePath =
            sprintf "%s/%s.mp3"
                newFileFolder
                soundName

        let! folderExist = rnfs.exists newFileFolder
        if not folderExist
        then do! rnfs.mkdir newFileFolder

        let download =
            rnfs.downloadFile
                { fromUrl = soundUrl
                  toFile = newFilePath }

        return! download.promise
    }

let downloadImage soundFolderPath soundName imageUrl =
    promise {
        let newFileFolder =
            sprintf "%s/%s"
                soundFolderPath
                soundName

        let newFilePath =
            sprintf "%s/%s.png"
                newFileFolder
                soundName

        let! folderExist = rnfs.exists newFileFolder
        if not folderExist
        then do! rnfs.mkdir newFileFolder

        let download =
            rnfs.downloadFile
                { fromUrl = imageUrl
                  toFile = newFilePath }

        return! download.promise
    }


let findNewSounds (localSounds: Fable.ReactNative.FileSystem.File []) (onlineSounds: API.Sounds.Sound []) =
    let newSounds =
        onlineSounds
        |> Array.filter (fun onlineSound ->
            localSounds
            |> Array.map (fun file -> file.name)
            |> Array.contains onlineSound.name
            |> not
        )

    let soundsToDelete =
        localSounds
        |> Array.filter (fun localSound ->
            onlineSounds
            |> Array.map (fun sound -> sound.name)
            |> Array.contains localSound.name
            |> not
        )

    (newSounds, soundsToDelete)

let updateLocalSounds soundFolderPath (soundsToDownload: API.Sounds.Sound []) (soundsToDelete: Fable.ReactNative.FileSystem.File []) =
    let deletePromises =
        soundsToDelete
        |> Array.collect (fun soundFile ->
            let imagePath =
                soundFile.path.Substring (soundFile.path.Length - 4) + ".png"

            [| soundFile.path |> rnfs.unlink
               imagePath |> rnfs.unlink |]
        )

    soundsToDownload
    |> Array.collect (fun sound ->
        [| downloadSound soundFolderPath sound.name sound.soundUri
           downloadImage soundFolderPath sound.name sound.imageUri |]
    )
    |> Array.append deletePromises
    |> Promise.all