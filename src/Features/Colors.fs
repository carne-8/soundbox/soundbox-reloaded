module Colors

type Colors =
    { Primary: string
      OnPrimary: string
      PrimaryContainer: string
      OnPrimaryContainer: string

      Secondary: string
      OnSecondary: string
      SecondaryContainer: string
      OnSecondaryContainer: string

      Tertiary: string
      OnTertiary: string
      TertiaryContainer: string
      OnTertiaryContainer: string

      Background: string
      OnBackground: string }

let getColors (dark: bool) : Colors =
    match dark with
    | false ->
        { Primary = "#00668A"
          OnPrimary = "#FFFFFF"
          PrimaryContainer = "#C0E8FF"
          OnPrimaryContainer = "#001E2C"

          Secondary = "#4E616D"
          OnSecondary = "#FFFFFF"
          SecondaryContainer = "#D0E5F3"
          OnSecondaryContainer = "#0A1E28"

          Tertiary = "#605A7D"
          OnTertiary = "#FFFFFF"
          TertiaryContainer = "#E6DEFF"
          OnTertiaryContainer = "#1C1736"

          Background = "#FBFCFE"
          OnBackground = "#191C1E" }
    | true ->
        { Primary = "#000000"
          OnPrimary = "#000000"
          PrimaryContainer = "#000000"
          OnPrimaryContainer = "#000000"

          Secondary = "#000000"
          OnSecondary = "#000000"
          SecondaryContainer = "#000000"
          OnSecondaryContainer = "#000000"

          Tertiary = "#000000"
          OnTertiary = "#000000"
          TertiaryContainer = "#000000"
          OnTertiaryContainer = "#000000"

          Background = "#000000"
          OnBackground = "#000000" }