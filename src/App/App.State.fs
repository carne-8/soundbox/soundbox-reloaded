namespace App

open Elmish
open Fable.Core.JsInterop
open Fable.ReactNative

module Cmds =
    open FileSystem
    open Sound
    open SoundManager

    let updateSounds () =
        Cmd.ofSub (fun dispatch ->
            promise {
                let soundsPath =
                    rnfs.DocumentDirectoryPath + "/sounds"

                let! soundsFolderExist = rnfs.exists soundsPath

                if not soundsFolderExist then
                    do! rnfs.mkdir soundsPath

                let! localSounds =
                    rnfs.readDir soundsPath

                let! onlineSoundsResult = API.Sounds.fetchSounds()

                match onlineSoundsResult with
                | Ok onlineSounds ->

                    let (newSounds, oldSounds) =
                        SoundManager.findNewSounds
                            localSounds
                            onlineSounds

                    let! results =
                        if newSounds.Length > 0 || oldSounds.Length > 0 then
                            SoundManager.updateLocalSounds
                                soundsPath
                                newSounds
                                oldSounds
                        else [| |] |> Promise.lift

                    results
                    |> Array.choose (fun result ->
                        match result with
                        | Ok _ -> None
                        | Error error -> Some error
                    )
                    |> Msg.SoundsUpdated
                    |> dispatch

                | Error error ->
                    printfn "first errors: %A" error
            } |> Promise.start
        )

    let chargeSounds () =
        Cmd.ofSub (fun dispatch ->
            promise {
                let! soundFolders =
                    rnfs.readDir (rnfs.DocumentDirectoryPath + "/sounds")

                soundFolders
                |> Array.map (fun folder ->
                    let soundPath = sprintf "%s/%s.mp3" folder.path folder.name
                    let imagePath = sprintf "%s/%s.png" folder.path folder.name

                    let sound = Sound.createSound soundPath

                    { Name = folder.name
                      Sound = sound
                      Image = imagePath
                      IsPlaying = false }
                )
                |> Msg.SoundsCharged
                |> dispatch
            } |> Promise.start
        )

    let playSound sound =
        Cmd.ofSub (fun dispatch ->
            match sound.IsPlaying with
            | false ->
                sound.Sound.play (fun _ -> sound |> Msg.SoundFinished |> dispatch)
            | true ->
                sound.Sound.stop (fun _ -> sound |> Msg.SoundFinished |> dispatch)
        )

module State =
    open Fable.Core

    let init () =
        SplashScreen.SplashScreen.hide()
        RN.StatusBar.setHidden
            true
            Types.StatusBarAnimation.Slide

        { Sounds = Loading None }, Cmds.updateSounds()

    let update msg model =
        match msg with
        | Msg.SoundsUpdated errors ->
            if errors |> Array.isEmpty
            then model, Cmds.chargeSounds()
            else model, Cmd.none

        | Msg.SoundsCharged sounds ->
            { model with Sounds = sounds |> Loaded }, Cmd.none

        // | Msg.PlaySound sound ->
        //     match model.Sounds with
        //     | Loaded sounds ->
        //         let newSound =
        //             match sound.IsPlaying with
        //             | false ->
        //                 sound.Sound.play(fun _ -> sound.IsPlaying <- false)
        //                 { sound with IsPlaying = true }
        //             | true ->
        //                 sound.Sound.stop()
        //                 { sound with IsPlaying = false }

        //         let newSoundArray =
        //             sounds
        //             |> Array.map (fun sound ->
        //                 if sound.Name = newSound.Name
        //                 then newSound
        //                 else sound
        //             )

        //         { model with Sounds = newSoundArray |> Loaded }, Cmd.none
        //     | _ -> model, Cmd.none

        | Msg.PlaySound sound ->
            match model.Sounds with
            | Loaded sounds ->
                let newSoundArray =
                    sounds
                    |> Array.map (fun x ->
                        if x.Name = sound.Name
                        then { x with IsPlaying = true }
                        else x
                    )

                { model with Sounds = newSoundArray |> Loaded }, Cmds.playSound sound
            | _ -> model, Cmd.none

        | Msg.SoundFinished sound ->
            match model.Sounds with
            | Loaded sounds ->
                let newSoundArray =
                    sounds
                    |> Array.map (fun x ->
                        if x.Name = sound.Name
                        then { x with IsPlaying = false }
                        else x
                    )

                { model with Sounds = newSoundArray |> Loaded }, Cmd.none
            | _ -> model, Cmd.none