[<AutoOpen>]
module YoLo

type Loading<'A> =
    | Loaded of 'A
    | Loading of 'A option

[<AutoOpen>]
module Fable =
    [<AutoOpen>]
    module ReactNative =
        [<AutoOpen>]
        module Helpers =
            open Fable.Core
            open Fable.ReactNative.Props

            [<Emit "\"auto\"">]
            let sizeAuto: ISizeUnit = jsNative