namespace Feliz

open Feliz
open Fable.Core
open Fable.Core.JsInterop

[<Erase>]
type ReactNative =
    static member inline view props children = Interop.reactApi.createElement (import "View" "react-native", createObj !!props, children)
    static member inline scrollView props children = Interop.reactApi.createElement (import "ScrollView" "react-native", createObj !!props, children)
    static member inline image props = Interop.reactApi.createElement (import "Image" "react-native", createObj !!props)

module ReactNative =
    [<Erase>]
    type prop =
        /// Sets the style of an element.
        static member inline style (properties: #IStyleAttribute list) = Interop.mkAttr "style" (createObj !!properties)
        /// Sets the image source
        static member inline source (value: {| uri: string|}) = Interop.mkAttr "source" value