namespace Feliz.ReactNative

open Feliz
open Fable.Core

[<Erase>]
type style =
    /// Sets the background color of an element.
    static member inline backgroundColor (color: string) = Interop.mkStyle "backgroundColor" color

    // Sets the width of an element.
    static member inline width (value: int) = Interop.mkStyle "width" value
    // Sets the width of an element.
    static member inline width (value: float) = Interop.mkStyle "width" value
    // Sets the height of an element.
    static member inline height (value: int) = Interop.mkStyle "height" value
    // Sets the height of an element.
    static member inline height (value: float) = Interop.mkStyle "height" value

    static member inline borderBottomColor (color: string) = Interop.mkStyle "height" color
    static member inline borderBottomEndRadius (value: int) = Interop.mkStyle "height" value
    static member inline borderBottomLeftRadius (value: int) = Interop.mkStyle "height" value
    static member inline borderBottomRightRadius (value: int) = Interop.mkStyle "height" value
    static member inline borderBottomStartRadius (value: int) = Interop.mkStyle "height" value
    static member inline borderBottomWidth (value: int) = Interop.mkStyle "height" value
    static member inline borderColor (color: string) = Interop.mkStyle "height" color