module Fable.ReactNative.Vibration

open Fable.Core

type Vibration =
    abstract cancel: unit -> unit
    abstract vibrate: int [] * repeat: bool -> unit
    abstract vibrate: int * repeat: bool -> unit

[<Import("Vibration", "react-native")>]
let Vibration: Vibration = jsNative