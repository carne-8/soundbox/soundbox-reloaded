module Fable.ReactNative.FileSystem

open Fable.Core

type DownloadOptions =
    { fromUrl: string
      toFile: string }

type File =
    { name: string
      path: string }

type DownloadResult =
    { jobId: int
      statusCode: int
      bytesWritten: int }

type ReactNativeFileSystem =
    abstract DocumentDirectoryPath: string
    abstract readDir: string -> JS.Promise<File []>
    abstract downloadFile:
        DownloadOptions ->
        {| jobId: int
           promise: JS.Promise<Result<obj, obj>> |}
    abstract unlink: path: string -> JS.Promise<Result<obj, obj>>
    abstract exists: path: string -> JS.Promise<bool>
    abstract mkdir: path: string -> JS.Promise<unit>

[<ImportDefault("react-native-fs")>]
let rnfs: ReactNativeFileSystem = jsNative