namespace Fable.ReactNative

open Fable.Core
open Fable.Core.JsInterop

open Fable.React
open Fable.ReactNative
open Fable.ReactNative.Props

module Types =
    [<StringEnum>]
    type ColorScheme =
        | Light
        | Dark

    type Appearance =
        abstract getColorScheme : unit -> ColorScheme
        abstract addChangeListener : ({| colorScheme: ColorScheme |} -> unit) -> unit

module Props =
    type TouchableWithoutFeedbackProperties =
        | TouchSoundDisabled of bool

        interface ITouchableWithoutFeedbackProperties


[<Erase>]
module RN =
    [<Import("Appearance", "react-native")>]
    let Appearance : Types.Appearance = jsNative