module Fable.ReactNative.SplashScreen

open Fable.Core

type SplashScreen =
    abstract member hide: unit -> unit

[<ImportDefault "react-native-splash-screen">]
let SplashScreen : SplashScreen = jsNative