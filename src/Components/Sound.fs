module Components.Sound

open Feliz
open Feliz.ReactNative
open Feliz.ReactNative.Animation

open Fable.ReactNative

let inline startAnimation (animation: AnimationFunc) = animation.start()

let screenSize = RN.Dimensions.get("screen")

[<ReactComponent>]
let View (props: {| Sound: SoundManager.Sound; OnPress: SoundManager.Sound -> unit |}) =
    let (scale, setScale) = React.useState (Anim.Value 1.)

    let margin = 15.
    let padding = 15.

    let width = (screenSize.width - 4. * margin) / 3.
    let height = 21. * width / 29.

    let colors = Colors.getColors false

    Comp.pressable [
        prop.onPressIn (fun _ ->
            { toValue = 0.95
              duration = 100
              useNativeDriver = true }
            |> Anim.timing scale
            |> startAnimation

            Vibration.Vibration.vibrate (20, false)
        )
        prop.onPressOut (fun _ ->
            { toValue = 1.
              bounciness = 18
              speed = 100
              useNativeDriver = true }
            |> Anim.spring scale
            |> startAnimation

            Vibration.Vibration.vibrate (20, false)
        )
        prop.onPress (fun _ -> props.Sound |> props.OnPress)
        prop.androidDisableSound true
        prop.children [
            Comp.viewAnimated [
                prop.style [
                    style.transform [ Transform.scale scale ]

                    style.backgroundColor colors.PrimaryContainer

                    style.borderRadius 24.

                    style.width width
                    style.height height

                    style.marginHorizontal (margin / 2.)
                    style.marginBottom margin

                    style.justifyContent.center
                    style.alignItems.center
                ]
                prop.children [
                    Comp.image [
                        prop.style [
                            style.width (height - padding)
                            style.height (height - padding)
                        ]
                        prop.source [ ImageSource.prop.uri ("file://" + props.Sound.Image) ]
                    ]
                ]
            ]
        ]
    ]