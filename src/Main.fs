module Main

open Elmish
open Elmish.React
open Elmish.ReactNative

open Elmish.Debug

open Fable.Core

[<Emit "typeof atob !== 'undefined'">]
let isRemoteDebug: bool = jsNative

let program =
    Program.mkProgram App.State.init App.State.update App.View.View
    |> Program.withReactNative "SoundBox"

match isRemoteDebug with
| true -> program |> Program.withDebugger
| false -> program
|> Program.run